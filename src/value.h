#ifndef LUACASS_VALUE_H
#define LUACASS_VALUE_H
#include "structs.h"
#include "helpers.h"
LUALIB_API void lua_cass_value_push(lua_State *L, const char *name, const CassValue *value, CassValueType type);
LUALIB_API int lua_cass_value_push_string(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_ascii(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_bigint(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_blob(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_boolean(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_counter(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_decimal(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_double(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_float(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_int(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_text(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_timestamp(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_uuid(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_varchar(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_varint(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_timeuuid(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_inet(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_date(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_time(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_small_int(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_tiny_int(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_list(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_map(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_set(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_udt(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_tuple(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_unknown(lua_State *L, const CassValue *value);
LUALIB_API int lua_cass_value_push_custom(lua_State *L, const CassValue *value);
#endif
