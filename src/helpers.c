#include "helpers.h"

#if !defined LUA_VERSION_NUM || LUA_VERSION_NUM==501
LUALIB_API void luaL_setfuncs (lua_State *L, const luaL_Reg *l, int nup) {
  luaL_checkstack(L, nup+1, "too many upvalues");
  for (; l->name != NULL; l++) {  /* fill the table with given functions */
    int i;
    lua_pushstring(L, l->name);
    for (i = 0; i < nup; i++)  /* copy upvalues to the top */
      lua_pushvalue(L, -(nup+1));
    lua_pushcclosure(L, l->func, nup);  /* closure with those upvalues */
    lua_settable(L, -(nup + 3));
  }
  lua_pop(L, nup);  /* remove upvalues */
}
#endif

LUALIB_API void setmeta(lua_State *L, const char *name) {
  luaL_getmetatable(L, name);
  lua_setmetatable(L, -2);
}

LUALIB_API int createmeta(lua_State *L, const char *name, const luaL_Reg *methods) {
  if (!luaL_newmetatable(L, name)) {
    return 0;
  }

  lua_pushstring(L, "__index");
  lua_newtable(L);
  lua_pushstring(L, "class");
  lua_pushstring(L, name);
  lua_rawset(L, -3);
  for (; methods->name; methods++) {
    lua_pushstring(L, methods->name);
    lua_pushcfunction(L, methods->func);
    lua_rawset(L, methods->name[0] == '_' ? -5: -3);
  }
  lua_rawset(L, -3);
  lua_pop(L, 1);
  return 1;
}

LUALIB_API cluster_t *getcluster(lua_State *L, size_t index) {
  cluster_t *c = (cluster_t *) luaL_checkudata(L, index, "cluster");
  luaL_argcheck(L, c != NULL && c->cluster != NULL, index, "cluster expected");
  return c;
}

LUALIB_API session_t *getsession(lua_State *L, size_t index) {
  session_t *s = (session_t *) luaL_checkudata(L, index, "session");
  luaL_argcheck(L, s != NULL && s->session != NULL, index, "session expected");
  return s;
}

LUALIB_API statement_t *getstatement(lua_State *L, size_t index) {
  statement_t *s = (statement_t *) luaL_checkudata(L, index, "statement");
  luaL_argcheck(L, s != NULL && s->statement != NULL, index, "statement expected");
  return s;
}

LUALIB_API result_t *getresult(lua_State *L, size_t index) {
  result_t *r = (result_t *) luaL_checkudata(L, index, "result");
  luaL_argcheck(L, r != NULL && r->result != NULL, index, "result expected");
  return r;
}

LUALIB_API iterator_t *getiterator(lua_State *L, size_t index) {
  iterator_t *i = (iterator_t *) luaL_checkudata(L, index, "iterator");
  luaL_argcheck(L, i != NULL && i->iterator != NULL, index, "iterator expected");
  return i;
}

LUALIB_API ssl_t *getssl(lua_State *L, size_t index) {
  ssl_t *s = (ssl_t *) luaL_checkudata(L, index, "ssl");
  luaL_argcheck(L, s != NULL && s->ssl != NULL, index, "ssl expected");
  return s;
}

LUALIB_API batch_t *getbatch(lua_State *L, size_t index) {
  batch_t *b = (batch_t *) luaL_checkudata(L, index, "batch");
  luaL_argcheck(L, b != NULL && b->batch != NULL, index, "batch expected");
  return b;
}


/*
 * Convert string to cass_int64_t
*/
LUALIB_API char *lltoa(long long val) {
  static char buf[64] = {0};
  int i = 62;
  int sign = (val < 0);
  if(sign) val = -val;
  if(val == 0) return "0";
  for(; val && i ; --i, val /= 10) {
    buf[i] = "0123456789abcdef"[val % 10];
  }
  if(sign) {
    buf[i--] = '-';
  }
  return &buf[i+1];
}

LUALIB_API cass_int64_t getint64(lua_State *L, size_t index) {
  const char *str_value = luaL_checkstring(L, index);
  cass_int64_t value = atoll(str_value);
  return value;
}



LUALIB_API CassInet getinet(lua_State *L, size_t index) {
  CassInet inet;
  const char *inet_str = luaL_checkstring(L, index);
  cass_inet_from_string(inet_str, &inet);
  return inet;
}

LUALIB_API CassUuid getuuid(lua_State *L, size_t index) {
  CassUuid uuid;
  const char *uuid_str = luaL_checkstring(L, index);
  cass_uuid_from_string(uuid_str, &uuid);
  return uuid;
}




