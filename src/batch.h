#ifndef LUACASS_BATCH_H
#define LUACASS_BATCH_H

#include "lua.h"
#include "lauxlib.h"
#include <cassandra.h>
#include "structs.h"
#include "helpers.h"
#include "statement.h"

LUALIB_API int lua_cass_batch_open(lua_State *L);

LUALIB_API int lua_cass_batch_new(lua_State *L);
LUALIB_API int lua_cass_batch_free(lua_State *L);
LUALIB_API int lua_cass_batch_add_statement(lua_State *L);

static const struct luaL_reg batch_reg[] = {
  { "new", lua_cass_batch_new },
  { "free", lua_cass_batch_free },
  { "add_statement", lua_cass_batch_add_statement },
  { "__gc", lua_cass_batch_free },
  { NULL, NULL }
};

#endif

