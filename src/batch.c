#include "batch.h"


LUALIB_API int lua_cass_batch_new(lua_State *L) {
  batch_t *b = (batch_t *)lua_newuserdata(L, sizeof(batch_t));
  b->batch = cass_batch_new(CASS_BATCH_TYPE_LOGGED); //TODO: Add constants
  setmeta(L, "batch");
  return 1;

  return 1;
}
LUALIB_API int lua_cass_batch_free(lua_State *L) {
  batch_t *b = getbatch(L, 1);
  if(b != NULL && b->batch!= NULL) {
    cass_batch_free(b->batch);
    b->batch = NULL;
  }
  return 0;
}

LUALIB_API int lua_cass_batch_add_statement(lua_State *L) {
  batch_t *b = getbatch(L, 1);
  statement_t *s = getstatement(L, 2);
  CassError rc = cass_batch_add_statement(b->batch, s->statement);
  if(rc == CASS_OK) {
    lua_pushboolean(L, 1);
  }
  else {
    lua_pushboolean(L, 0);
  }
  return 1;
}

static luaL_Reg batch_module[] = {
  { "batch", lua_cass_batch_new },
  { NULL, NULL }
};

LUALIB_API int lua_cass_batch_open(lua_State *L) {
  createmeta(L, "batch", batch_reg);
  luaL_setfuncs(L, batch_module, 0);
  return 0;
}

