#ifndef LUACASS_SSL_H
#define LUACASS_SSL_H

#include "lua.h"
#include "lauxlib.h"
#include <cassandra.h>
#include "structs.h"
#include "helpers.h"

LUALIB_API int lua_cass_ssl_open(lua_State *L);
LUALIB_API int lua_cass_ssl_new(lua_State *L);
LUALIB_API int lua_cass_ssl_free(lua_State *L);
LUALIB_API int lua_cass_ssl_add_trusted_cert(lua_State *L);
LUALIB_API int lua_cass_ssl_set_verify_flags(lua_State *L);
LUALIB_API int lua_cass_ssl_set_cert(lua_State *L);
LUALIB_API int lua_cass_ssl_set_private_key(lua_State *L);

static const struct luaL_reg ssl_reg[] = {
  { "new", lua_cass_ssl_new },
  { "free", lua_cass_ssl_free },
  { "add_trusted_cert", lua_cass_ssl_add_trusted_cert },
  { "set_cert", lua_cass_ssl_set_cert },
  { "set_verify_flags", lua_cass_ssl_set_verify_flags },
  { "set_private_key", lua_cass_ssl_set_private_key },
  { "__gc", lua_cass_ssl_free },
  { NULL, NULL }
};

#endif

