#ifndef LUACASS_STATEMENT_H
#define LUACASS_STATEMENT_H

#include "lua.h"
#include "lauxlib.h"
#include <cassandra.h>
#include "structs.h"
#include "helpers.h"

LUALIB_API int lua_cass_statement_open(lua_State *L);
LUALIB_API int lua_cass_statement_new(lua_State *L);
LUALIB_API int lua_cass_statement_new_from_prepared(lua_State *L, const CassPrepared *prepared);
LUALIB_API int lua_cass_statement_free(lua_State *L);
LUALIB_API int lua_cass_statement_add_key_index(lua_State *L);
LUALIB_API int lua_cass_statement_set_keyspace(lua_State *L);
LUALIB_API int lua_cass_statement_set_consistency(lua_State *L);
LUALIB_API int lua_cass_statement_set_serial_consistency(lua_State *L);
LUALIB_API int lua_cass_statement_set_paging_size(lua_State *L);
LUALIB_API int lua_cass_statement_set_paging_state(lua_State *L);
LUALIB_API int lua_cass_statement_set_paging_state_token(lua_State *L);
LUALIB_API int lua_cass_statement_set_timestamp(lua_State *L);
LUALIB_API int lua_cass_statement_set_retry_policy(lua_State *L);
LUALIB_API int lua_cass_statement_set_custom_payload(lua_State *L);
LUALIB_API int lua_cass_statement_bind_null(lua_State *L);
LUALIB_API int lua_cass_statement_bind_null_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_int8(lua_State *L);
LUALIB_API int lua_cass_statement_bind_int8_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_int16(lua_State *L);
LUALIB_API int lua_cass_statement_bind_int16_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_int32(lua_State *L);
LUALIB_API int lua_cass_statement_bind_int32_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_uint32(lua_State *L);
LUALIB_API int lua_cass_statement_bind_uint32_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_int64(lua_State *L);
LUALIB_API int lua_cass_statement_bind_int64_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_float(lua_State *L);
LUALIB_API int lua_cass_statement_bind_float_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_double(lua_State *L);
LUALIB_API int lua_cass_statement_bind_double_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_bool(lua_State *L);
LUALIB_API int lua_cass_statement_bind_bool_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_string(lua_State *L);
LUALIB_API int lua_cass_statement_bind_string_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_bytes(lua_State *L);
LUALIB_API int lua_cass_statement_bind_bytes_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_uuid(lua_State *L);
LUALIB_API int lua_cass_statement_bind_uuid_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_inet(lua_State *L);
LUALIB_API int lua_cass_statement_bind_inet_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_decimal(lua_State *L);
LUALIB_API int lua_cass_statement_bind_decimal_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_collection(lua_State *L);
LUALIB_API int lua_cass_statement_bind_collection_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_tuple(lua_State *L);
LUALIB_API int lua_cass_statement_bind_tuple_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind_user_type(lua_State *L);
LUALIB_API int lua_cass_statement_bind_user_type_by_name(lua_State *L);
LUALIB_API int lua_cass_statement_bind(lua_State *L);

static const luaL_reg statement_reg[] = {
  { "new", lua_cass_statement_new },
  { "free", lua_cass_statement_free },
  { "add_key_index", lua_cass_statement_add_key_index },
  { "set_keyspace", lua_cass_statement_set_keyspace },
  { "set_consistency", lua_cass_statement_set_consistency },
  { "set_serial_consistency", lua_cass_statement_set_serial_consistency },
  { "set_paging_size", lua_cass_statement_set_paging_size },
  { "set_paging_state", lua_cass_statement_set_paging_state },
  { "set_paging_state_token", lua_cass_statement_set_paging_state_token },
  { "set_timestamp", lua_cass_statement_set_timestamp },
  { "set_retry_policy", lua_cass_statement_set_retry_policy },
  { "set_custom_payload", lua_cass_statement_set_custom_payload },
  { "bind_null", lua_cass_statement_bind_null },
  { "bind_null_by_name", lua_cass_statement_bind_null_by_name },
  { "bind_int8", lua_cass_statement_bind_int8 },
  { "bind_int8_by_name", lua_cass_statement_bind_int8_by_name },
  { "bind_int16", lua_cass_statement_bind_int16 },
  { "bind_int16_by_name", lua_cass_statement_bind_int16_by_name },
  { "bind_int32", lua_cass_statement_bind_int32 },
  { "bind_int32_by_name", lua_cass_statement_bind_int32_by_name },
  { "bind_uint32", lua_cass_statement_bind_uint32 },
  { "bind_uint32_by_name", lua_cass_statement_bind_uint32_by_name },
  { "bind_int64", lua_cass_statement_bind_int64 },
  { "bind_int64_by_name", lua_cass_statement_bind_int64_by_name },
  { "bind_float", lua_cass_statement_bind_float },
  { "bind_float_by_name", lua_cass_statement_bind_float_by_name },
  { "bind_double", lua_cass_statement_bind_double },
  { "bind_double_by_name", lua_cass_statement_bind_double_by_name },
  { "bind_bool", lua_cass_statement_bind_bool },
  { "bind_bool_by_name", lua_cass_statement_bind_bool_by_name },
  { "bind_string", lua_cass_statement_bind_string },
  { "bind_string_by_name", lua_cass_statement_bind_string_by_name },
  { "bind_bytes", lua_cass_statement_bind_bytes },
  { "bind_bytes_by_name", lua_cass_statement_bind_bytes_by_name },
  { "bind_uuid", lua_cass_statement_bind_uuid },
  { "bind_uuid_by_name", lua_cass_statement_bind_uuid_by_name },
  { "bind_inet", lua_cass_statement_bind_inet },
  { "bind_inet_by_name", lua_cass_statement_bind_inet_by_name },
  { "bind_decimal", lua_cass_statement_bind_decimal },
  { "bind_decimal_by_name", lua_cass_statement_bind_decimal_by_name },
  { "bind_collection", lua_cass_statement_bind_collection },
  { "bind_collection_by_name", lua_cass_statement_bind_collection_by_name },
  { "bind_tuple", lua_cass_statement_bind_tuple },
  { "bind_tuple_by_name", lua_cass_statement_bind_tuple_by_name },
  { "bind_user_type", lua_cass_statement_bind_user_type },
  { "bind_user_type_by_name", lua_cass_statement_bind_user_type_by_name },
  { "bind", lua_cass_statement_bind },
  { "__gc", lua_cass_statement_free },
  { NULL, NULL }
};


#endif

