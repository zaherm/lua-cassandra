#ifndef LUACASS_SESSION_H
#define LUACASS_SESSION_H

#include "lua.h"
#include "lauxlib.h"
#include <cassandra.h>
#include "structs.h"
#include "helpers.h"
#include "result.h"
#include "statement.h"
#include "batch.h"


LUALIB_API int lua_cass_session_open(lua_State *L);
LUALIB_API int lua_cass_session_new(lua_State *L);
LUALIB_API int lua_cass_session_free(lua_State *L);
LUALIB_API int lua_cass_session_connect(lua_State *L);
LUALIB_API int lua_cass_session_connect_keyspace(lua_State *L);
LUALIB_API int lua_cass_session_close(lua_State *L);
LUALIB_API int lua_cass_session_prepare(lua_State *L);
LUALIB_API int lua_cass_session_execute(lua_State *L);
LUALIB_API int lua_cass_session_execute_batch(lua_State *L);
LUALIB_API int lua_cass_session_get_schema_meta(lua_State *L);
LUALIB_API int lua_cass_session_get_metrics(lua_State *L);


static const struct luaL_reg session_reg[] = {
  { "new", lua_cass_session_new },
  { "free", lua_cass_session_free },
  { "connect", lua_cass_session_connect },
  { "connect_keyspace", lua_cass_session_connect_keyspace },
  { "close", lua_cass_session_close },
  { "prepare", lua_cass_session_prepare },
  { "execute", lua_cass_session_execute },
  { "execute_batch", lua_cass_session_execute_batch },
  { "get_schema_meta", lua_cass_session_get_schema_meta },
  { "get_metrics", lua_cass_session_get_metrics },
  { "__gc", lua_cass_session_free },
  { NULL, NULL }
};

#endif

