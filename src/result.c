#include "result.h"
/*
* CassCluster * cass_cluster_new()
*/
LUALIB_API int lua_cass_result_new(lua_State *L, CassFuture *future) {
  result_t *r = (result_t *)lua_newuserdata(L, sizeof(result_t));
  r->result = cass_future_get_result(future);
  size_t column_count = cass_result_column_count(r->result);
  r->columns = malloc((column_count + 1) * sizeof(column_t));
  size_t index, name_length;
  for(index = 0; index < column_count; index++) {
    cass_result_column_name(r->result, index, &r->columns[index].name, &name_length);
    r->columns[index].type = cass_result_column_type(r->result, index);
  }
  r->columns[index].name = NULL;
  setmeta(L, "result");
  return 1;
}

/*
* void cass_result_free(const CassResult * result)
*/
LUALIB_API int lua_cass_result_free(lua_State *L) {
  result_t *r = getresult(L, 1);
  if(r != NULL && r->result != NULL) {
    cass_result_free(r->result);
    r->result = NULL;
  }
  return 0;
}

/*
* size_t cass_result_row_count(const CassResult * result)
*/
LUALIB_API int lua_cass_result_row_count(lua_State *L) {
  result_t *r = getresult(L, 1);
  size_t row_count = cass_result_row_count(r->result);
  lua_pushnumber(L, row_count);
  return 1;
}

/*
* size_t cass_result_column_count(const CassResult * result)
*/
LUALIB_API int lua_cass_result_column_count(lua_State *L) {
  result_t *r = getresult(L, 1);
  size_t column_count = cass_result_column_count(r->result);
  lua_pushnumber(L, column_count);
  return 1;
}

/*
* CassError cass_result_column_name(const CassResult * result, size_t index, const char ** name, size_t * name_length)
*/
LUALIB_API int lua_cass_result_column_name(lua_State *L) {
  result_t *r = getresult(L, 1);
  size_t index = luaL_checknumber(L, 2);
  const char *name;
  size_t name_length;
  CassError ce = cass_result_column_name(r->result, index, &name, &name_length);
  if(ce == CASS_OK) {
    lua_pushlstring(L, name, name_length);
  }
  return 1;
}

/* TODO
*  CassValueType cass_result_column_type(const CassResult * result, size_t index)
*/

/* TODO
* const CassDataType * cass_result_column_data_type(const CassResult * result, size_t index)
*/

/* TODO
* const CassRow * cass_result_first_row(const CassResult * result)
*/

/*
* cass_bool_t cass_result_has_more_pages(const CassResult * result)
*/
LUALIB_API int lua_cass_result_has_more_pages(lua_State *L) {
  result_t *r = getresult(L, 1);
  cass_bool_t has_more_pages = cass_result_has_more_pages(r->result);
  lua_pushboolean(L, has_more_pages);
  return 1;
}

/*
* CassError cass_result_paging_state_token(const CassResult * result, const char ** paging_state, size_t * paging_state_size)
*/
LUALIB_API int lua_cass_result_paging_state_token(lua_State *L) {
  result_t *r = getresult(L, 1);
  const char *paging_state;
  size_t paging_state_size;
  CassError ce = cass_result_paging_state_token(r->result, &paging_state, &paging_state_size);
  if(ce == CASS_OK) {
    lua_pushlstring(L, paging_state, paging_state_size);
  }
  else {
    lua_pushboolean(L, 0);
  }
  return 1;
}

static luaL_Reg result_module[] = {
  { NULL, NULL }
};

LUALIB_API int lua_cass_result_open(lua_State *L) {
  createmeta(L, "result", result_reg);
  luaL_setfuncs(L, result_module, 0);
  return 0;
}

