#include "ssl.h"

LUALIB_API int lua_cass_ssl_new(lua_State *L) {
  ssl_t *s = (ssl_t *)lua_newuserdata(L, sizeof(ssl_t));
  s->ssl = cass_ssl_new();
  setmeta(L, "ssl");
  return 1;
}

LUALIB_API int lua_cass_ssl_free(lua_State *L) {
  ssl_t *s = getssl(L, 1);
  if(s != NULL && s->ssl != NULL) {
    cass_ssl_free(s->ssl);
    s->ssl = NULL;
  }
  return 0;
}

LUALIB_API int lua_cass_ssl_add_trusted_cert(lua_State *L) {
  ssl_t *s = getssl(L, 1);
  size_t cert_length;
  const char *cert = luaL_checklstring(L, 2, &cert_length);
  CassError rc = cass_ssl_add_trusted_cert_n(s->ssl, cert, cert_length);
  lua_pushboolean(L, (rc == CASS_OK ? 1 : 0));
  return 1;
}

LUALIB_API int lua_cass_ssl_set_verify_flags(lua_State *L) {
  luaL_error(L, "Not implemented yet");
  return 1;
}

LUALIB_API int lua_cass_ssl_set_cert(lua_State *L) {
  ssl_t *s = getssl(L, 1);
  size_t cert_length;
  const char *cert = luaL_checklstring(L, 2, &cert_length);
  CassError rc = cass_ssl_set_cert_n(s->ssl, cert, cert_length);
  lua_pushboolean(L, (rc == CASS_OK ? 1 : 0));
  return 1;
}

LUALIB_API int lua_cass_ssl_set_private_key(lua_State *L) {
  ssl_t *s = getssl(L, 1);
  size_t key_length;
  const char *key = luaL_checklstring(L, 2, &key_length);
  size_t password_length;
  const char *password = luaL_checklstring(L, 3, &password_length);
  CassError rc = cass_ssl_set_private_key_n(s->ssl, key, key_length, password, password_length);
  lua_pushboolean(L, (rc == CASS_OK ? 1 : 0));
  return 1;
}

static luaL_Reg ssl_module[] = {
  { "ssl", lua_cass_ssl_new },
  { NULL, NULL }
};

LUALIB_API int lua_cass_ssl_open(lua_State *L) {
  createmeta(L, "ssl", ssl_reg);
  luaL_setfuncs(L, ssl_module, 0);
  return 0;
}

