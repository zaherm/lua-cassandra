#include "lua_cassandra.h"

static const luaL_Reg modules[] = {
  { "cluster", lua_cass_cluster_open },
  { "statement", lua_cass_statement_open },
  { "session", lua_cass_session_open },
  { "result", lua_cass_result_open },
  { "iterator", lua_cass_iterator_open },
  { "ssl", lua_cass_ssl_open },
  { "batch", lua_cass_batch_open },
  {NULL, NULL}
};

LUALIB_API int lua_cass_gc(lua_State *L) {
  lua_pushnumber(L, 1);
  return 1;
}

LUALIB_API int luaopen_cassandra(lua_State *L) {
  lua_newtable(L);
  int i;
  for (i = 0; modules[i].name; i++) {
    modules[i].func(L);
  }
  lua_pushliteral(L, LUACASS_VERSION);
  lua_setfield(L, -2, "_VERSION");
  lua_pushliteral(L, LUACASS_COPYRIGHT);
  lua_setfield(L, -2, "_COPYRIGHT");
  lua_pushliteral(L, LUACASS_DESCRIPTION);
  lua_setfield(L, -2, "_DESCRIPTION");

  return 1;
}

