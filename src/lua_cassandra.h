#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "lauxlib.h"
#include <cassandra.h>
#include "helpers.h"
#include "cluster.h"
#include "session.h"
#include "statement.h"
#include "result.h"
#include "iterator.h"
#include "ssl.h"
#include "batch.h"
#define LUACASS_VERSION "lua-cassandra 0.0.1"
#define LUACASS_COPYRIGHT "Copyright (C) 2016, Zaher Marzuq"
#define LUACASS_DESCRIPTION "Cassandra binding for Lua"

LUALIB_API int lua_cass_gc(lua_State *L);

