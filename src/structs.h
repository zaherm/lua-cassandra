#ifndef LUACASS_STRUCTS_H
#define LUACASS_STRUCTS_H

#include <cassandra.h>

typedef struct {
  CassCluster *cluster;
} cluster_t;

typedef struct {
 CassSession *session;
} session_t;

typedef struct {
  CassStatement *statement;
  const CassPrepared *prepared;
} statement_t;

typedef struct {
  const char *name;
  CassValueType type;
} column_t;

typedef struct {
  const CassResult *result;
  column_t *columns;
} result_t;

typedef struct {
  CassIterator *iterator;
  result_t *r;
} iterator_t;

typedef struct {
  CassSsl *ssl;
} ssl_t;

typedef struct {
  CassBatch *batch;
} batch_t;

#endif

