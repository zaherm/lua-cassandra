#include "iterator.h"
#include "value.h"
/*
* CassIterator * cass_iterator_from_result(const CassResult * result)
*/

LUALIB_API int lua_cass_iterator_from_result(lua_State *L) {
  result_t *r = getresult(L, 1);
  iterator_t *i = (iterator_t *)lua_newuserdata(L, sizeof(iterator_t));
  i->iterator = cass_iterator_from_result(r->result);
  i->r = r;
  setmeta(L, "iterator");
  return 1;
}

/*
* void cass_iterator_free(CassIterator * iterator)
*/
LUALIB_API int lua_cass_iterator_free(lua_State *L) {
  iterator_t *i = getiterator(L, 1);
  if(i != NULL && i->iterator != NULL) {
    cass_iterator_free(i->iterator);
    i->iterator = NULL;
  }
  return 0;
}

/*
* cass_bool_t cass_iterator_next(CassIterator * iterator)
*/

LUALIB_API int lua_cass_iterator_next(lua_State *L) {
  iterator_t *i = getiterator(L, 1);
  cass_bool_t next = cass_iterator_next(i->iterator);
  lua_pushboolean(L, next);
  return 1;
}

/* TODO
* const CassRow * cass_iterator_get_row(const CassIterator * iterator)
*/
LUALIB_API int lua_cass_iterator_get_row(lua_State *L) {
  iterator_t *i = getiterator(L, 1);
  if(i->r != NULL) {
    result_t *r = i->r;
    const CassRow *row = cass_iterator_get_row(i->iterator);
    if(row != NULL) {
      lua_newtable(L);
      size_t index = 0;
      const CassValue *value;
      CassValueType type;
      const char *name;
      for(;r->columns[index].name != NULL; index++) {
        name = r->columns[index].name;
        type = r->columns[index].type;
        value = cass_row_get_column_by_name(row, name);
        lua_cass_value_push(L, name, value, type);
      }
    }
  }
  else {
    luaL_error(L, "result is not defined");
  }
  return 1;
}

/* TODO
* const CassValue * cass_iterator_get_column(const CassIterator * iterator)
*/
LUALIB_API int lua_cass_iterator_get_column(lua_State *L) {
  return 1;
}

/* TODO
* const CassValue * cass_iterator_get_value(const CassIterator * iterator)
*/

LUALIB_API int lua_cass_iterator_get_value(lua_State *L) {
  return 1;
}

static luaL_Reg iterator_module[] = {
  { "iterator", lua_cass_iterator_from_result },
  { NULL, NULL }
};

LUALIB_API int lua_cass_iterator_open(lua_State *L) {
  createmeta(L, "iterator", iterator_reg);
  luaL_setfuncs(L, iterator_module, 0);
  return 0;
}

