#include "value.h"

LUALIB_API void lua_cass_value_push(lua_State *L, const char *name, const CassValue *value, CassValueType type) {
  int rc = 0;
  if (cass_value_is_null(value)){
    rc = lua_cass_value_push_unknown(L, value);
  }
  else{
    switch(type) {
      case CASS_VALUE_TYPE_UNKNOWN:
        rc = lua_cass_value_push_unknown(L, value);
        break;
      case CASS_VALUE_TYPE_CUSTOM:
        rc = lua_cass_value_push_custom(L, value);
        break;
      case CASS_VALUE_TYPE_ASCII:
        rc = lua_cass_value_push_ascii(L, value);
        break;
      case CASS_VALUE_TYPE_BIGINT:
        rc = lua_cass_value_push_bigint(L, value);
        break;
      case CASS_VALUE_TYPE_BLOB:
        rc = lua_cass_value_push_blob(L, value);
        break;
      case CASS_VALUE_TYPE_BOOLEAN:
        rc = lua_cass_value_push_boolean(L, value);
        break;
      case CASS_VALUE_TYPE_COUNTER:
        rc = lua_cass_value_push_counter(L, value);
        break;
      case CASS_VALUE_TYPE_DECIMAL:
        rc = lua_cass_value_push_decimal(L, value);
        break;
      case CASS_VALUE_TYPE_DOUBLE:
        rc = lua_cass_value_push_double(L, value);
        break;
      case CASS_VALUE_TYPE_FLOAT:
        rc = lua_cass_value_push_float(L, value);
        break;
      case CASS_VALUE_TYPE_INT:
        rc = lua_cass_value_push_int(L, value);
        break;
      case CASS_VALUE_TYPE_TEXT:
        rc = lua_cass_value_push_text(L, value);
        break;
      case CASS_VALUE_TYPE_TIMESTAMP:
        rc = lua_cass_value_push_timestamp(L, value);
        break;
      case CASS_VALUE_TYPE_UUID:
        rc = lua_cass_value_push_uuid(L, value);
        break;
      case CASS_VALUE_TYPE_VARCHAR:
        rc = lua_cass_value_push_varchar(L, value);
        break;
      case CASS_VALUE_TYPE_VARINT:
        rc = lua_cass_value_push_varint(L, value);
        break;
      case CASS_VALUE_TYPE_TIMEUUID:
        rc = lua_cass_value_push_timeuuid(L, value);
        break;
      case CASS_VALUE_TYPE_INET:
        rc = lua_cass_value_push_inet(L, value);
        break;
      case CASS_VALUE_TYPE_DATE:
        rc = lua_cass_value_push_date(L, value);
        break;
      case CASS_VALUE_TYPE_TIME:
        rc = lua_cass_value_push_time(L, value);
        break;
      case CASS_VALUE_TYPE_SMALL_INT:
        rc = lua_cass_value_push_small_int(L, value);
        break;
      case CASS_VALUE_TYPE_TINY_INT:
        rc = lua_cass_value_push_tiny_int(L, value);
        break;
      case CASS_VALUE_TYPE_LIST:
        rc = lua_cass_value_push_list(L, value);
        break;
      case CASS_VALUE_TYPE_MAP:
        rc = lua_cass_value_push_map(L, value);
        break;
      case CASS_VALUE_TYPE_SET:
        rc = lua_cass_value_push_set(L, value);
        break;
      case CASS_VALUE_TYPE_UDT:
        rc = lua_cass_value_push_udt(L, value);
        break;
      case CASS_VALUE_TYPE_TUPLE:
        rc = lua_cass_value_push_tuple(L, value);
        break;
      default:
        luaL_error(L, "unsupported value type");
    }
    if(rc) {
      lua_setfield(L, -2, name);
    }
  }
}

LUALIB_API int lua_cass_value_push_string(lua_State *L, const CassValue *value) {
  const char *output;
  size_t output_length;
  CassError rc = cass_value_get_string(value, &output, &output_length);
  if(rc == CASS_OK) {
    lua_pushlstring(L, output, output_length);
    return 1;
  }
  return 0;
}


LUALIB_API int lua_cass_value_push_ascii(lua_State *L, const CassValue *value) {
  return lua_cass_value_push_string(L, value);
}

LUALIB_API int lua_cass_value_push_bigint(lua_State *L, const CassValue *value) {
  cass_int64_t output;
  CassError rc = cass_value_get_int64(value, &output);
  if(rc == CASS_OK) {
    lua_pushnumber(L, output);
    return 1;
  }
  return 0;
}

LUALIB_API int lua_cass_value_push_blob(lua_State *L, const CassValue *value) {
  return 0;
}

LUALIB_API int lua_cass_value_push_boolean(lua_State *L, const CassValue *value) {
  cass_bool_t output;
  CassError rc = cass_value_get_bool(value, &output);
  if(rc == CASS_OK) {
    lua_pushboolean(L, output);
    return 1;
  }
  return 0;
}

LUALIB_API int lua_cass_value_push_counter(lua_State *L, const CassValue *value) {
  return lua_cass_value_push_bigint(L, value);
}

LUALIB_API int lua_cass_value_push_decimal(lua_State *L, const CassValue *value) {
  return 0;
}

LUALIB_API int lua_cass_value_push_double(lua_State *L, const CassValue *value) {
  cass_double_t output;
  CassError rc = cass_value_get_double(value, &output);
  if(rc == CASS_OK) {
    lua_pushnumber(L, output);
  }
  return 1;
}

LUALIB_API int lua_cass_value_push_float(lua_State *L, const CassValue *value) {
  cass_float_t output;
  CassError rc = cass_value_get_float(value, &output);
  if(rc == CASS_OK) {
    lua_pushnumber(L, output);
  }
  return 1;
}

LUALIB_API int lua_cass_value_push_int(lua_State *L, const CassValue *value) {
  cass_int32_t output;
  CassError rc = cass_value_get_int32(value, &output);
  if(rc == CASS_OK) {
    lua_pushnumber(L, output);
  }
  return 1;
}

LUALIB_API int lua_cass_value_push_text(lua_State *L, const CassValue *value) {
  lua_cass_value_push_string(L, value);
  return 1;
}

LUALIB_API int lua_cass_value_push_timestamp(lua_State *L, const CassValue *value) {
  lua_cass_value_push_bigint(L, value);
  return 1;
}

LUALIB_API int lua_cass_value_push_uuid(lua_State *L, const CassValue *value) {
  CassUuid output;
  char uuid[CASS_UUID_STRING_LENGTH];
  CassError rc = cass_value_get_uuid(value, &output);
  if(rc == CASS_OK) {
    cass_uuid_string(output, uuid);
    lua_pushstring(L, uuid);
  }
  return 1;
}

LUALIB_API int lua_cass_value_push_varchar(lua_State *L, const CassValue *value) {
  return lua_cass_value_push_string(L, value);
}

LUALIB_API int lua_cass_value_push_varint(lua_State *L, const CassValue *value) {
  return 0;
}

LUALIB_API int lua_cass_value_push_timeuuid(lua_State *L, const CassValue *value) {
  return lua_cass_value_push_uuid(L, value);
}

LUALIB_API int lua_cass_value_push_inet(lua_State *L, const CassValue *value) {
  CassInet output;
  char inet[CASS_INET_STRING_LENGTH];
  CassError rc = cass_value_get_inet(value, &output);
  if(rc == CASS_OK) {
    cass_inet_string(output, inet);
    lua_pushstring(L, inet);
    return 1;
  }
  return 0;
}

LUALIB_API int lua_cass_value_push_date(lua_State *L, const CassValue *value) {
  return 0;
}

LUALIB_API int lua_cass_value_push_time(lua_State *L, const CassValue *value) {
  return 0;
}

LUALIB_API int lua_cass_value_push_small_int(lua_State *L, const CassValue *value) {
  return 0;
}

LUALIB_API int lua_cass_value_push_tiny_int(lua_State *L, const CassValue *value) {
  return 0;
}

LUALIB_API int lua_cass_value_push_list(lua_State *L, const CassValue *value) {
  return 0;
}

LUALIB_API int lua_cass_value_push_map(lua_State *L, const CassValue *value) {
  return 0;
}

LUALIB_API int lua_cass_value_push_set(lua_State *L, const CassValue *value) {
  return 0;
}

LUALIB_API int lua_cass_value_push_udt(lua_State *L, const CassValue *value) {
  return 0;
}

LUALIB_API int lua_cass_value_push_tuple(lua_State *L, const CassValue *value) {
  return 0;
}

LUALIB_API int lua_cass_value_push_unknown(lua_State *L, const CassValue *value) {
  return 0;
}

LUALIB_API int lua_cass_value_push_custom(lua_State *L, const CassValue *value) {
  return 0;
}


