#ifndef LUACASS_ITERATOR_H
#define LUACASS_ITERATOR_H

#include "lua.h"
#include "lauxlib.h"
#include <cassandra.h>
#include "structs.h"
#include "helpers.h"

LUALIB_API int lua_cass_iterator_open(lua_State *L);
LUALIB_API void pushvalue(lua_State *L, const char *name, const CassValue *value, CassValueType type);
LUALIB_API int lua_cass_iterator_from_result(lua_State *L);
LUALIB_API int lua_cass_iterator_free(lua_State *L);
LUALIB_API int lua_cass_iterator_next(lua_State *L);
LUALIB_API int lua_cass_iterator_get_row(lua_State *L);
LUALIB_API int lua_cass_iterator_get_column(lua_State *L);
LUALIB_API int lua_cass_iterator_get_value(lua_State *L);

static const luaL_reg iterator_reg[] = {
  { "free", lua_cass_iterator_free },
  { "next", lua_cass_iterator_next },
  { "get_row", lua_cass_iterator_get_row },
  { "get_column", lua_cass_iterator_get_column },
  { "get_value", lua_cass_iterator_get_value },
  { "__gc", lua_cass_iterator_free },
  { NULL, NULL }
};

#endif


