#include "session.h"
/*
* CassSession* cass_session_new()
*/
LUALIB_API int lua_cass_session_new(lua_State *L) {
  session_t *s = (session_t *) lua_newuserdata(L, sizeof(session_t));
  s->session = cass_session_new();
  setmeta(L, "session");
  return 1;
}

/*
* void cass_session_free(CassSession* session)
*/
LUALIB_API int lua_cass_session_free(lua_State *L) {
  session_t *s = getsession(L, 1);
  if(s != NULL && s->session != NULL) {
    cass_session_free(s->session);
    s->session = NULL;
  }
  lua_pushnumber(L, 1);
  return 1;
}

/*
* CassFuture* cass_session_connect(CassSession* session, const CassCluster* cluster)
*/
LUALIB_API int lua_cass_session_connect(lua_State *L) {
  session_t *s = getsession(L, 1);
  cluster_t *c = getcluster(L, 2);
  CassFuture *future = cass_session_connect(s->session, c->cluster);
  cass_future_wait(future);
  if (cass_future_error_code(future) == CASS_OK) {
    lua_pushboolean(L, 1);
  }
  else {
    lua_pushboolean(L, 0);
  }
  cass_future_free(future);
  return 1;

}

/*
* CassFuture* cass_session_connect_keyspace(CassSession* session, const CassCluster* cluster, const char* keyspace)
*/
LUALIB_API int lua_cass_session_connect_keyspace(lua_State *L) {
  session_t *s = getsession(L, 1);
  cluster_t *c = getcluster(L, 2);
  const char *keyspace = lua_tostring(L, 3);
  CassFuture *future = cass_session_connect_keyspace(s->session, c->cluster, keyspace);
  cass_future_wait(future);
  if (cass_future_error_code(future) == CASS_OK) {
    lua_pushboolean(L, 1);
  }
  else {
    lua_pushboolean(L, 0);
  }
  cass_future_free(future);
  return 1;

}

/*
* CassFuture* cass_session_close(CassSession* session)
*/
LUALIB_API int lua_cass_session_close(lua_State *L) {
  session_t *s = getsession(L, 1);
  CassFuture *future = cass_session_close(s->session);
  cass_future_wait(future);
  if (cass_future_error_code(future) == CASS_OK) {
    lua_pushboolean(L, 1);
  }
  else {
    lua_pushboolean(L, 0);
  }
  cass_future_free(future);
  return 1;

}

/*
* CassFuture* cass_session_prepare(CassSession* session, const char* query)
*/
LUALIB_API int lua_cass_session_prepare(lua_State *L) {
  session_t *s = getsession(L, 1);
  const char *query = lua_tostring(L, 2);
  CassFuture *future = cass_session_prepare(s->session, query);
  cass_future_wait(future);
  if (cass_future_error_code(future) == CASS_OK) {
    const CassPrepared *prepared = cass_future_get_prepared(future);
    lua_cass_statement_new_from_prepared(L, prepared);
  }
  else {
    lua_pushnil(L);
  }
  cass_future_free(future);
  return 1;

}

/*
* CassFuture* cass_session_execute(CassSession* session, const CassStatement* statement)
*/
LUALIB_API int lua_cass_session_execute(lua_State *L) {
  session_t *s = getsession(L, 1);
  statement_t *st = getstatement(L, 2);
  CassFuture *future = cass_session_execute(s->session, st->statement);
  cass_future_wait(future);
  if (cass_future_error_code(future) == CASS_OK) {
    lua_cass_result_new(L, future);
  }
  else{
    const char* message;
    size_t message_length;
    cass_future_error_message(future, &message, &message_length);
    fprintf(stderr, "Unable to run query: '%.*s'\n",
        (int)message_length, message);
    lua_pushboolean(L, 0);
  }
  cass_future_free(future);
  return 1;

}

/*
* CassFuture* cass_session_execute_batch(CassSession* session, const CassBatch* batch)
*/
LUALIB_API int lua_cass_session_execute_batch(lua_State *L) {
  session_t *s = getsession(L, 1);
  batch_t *b = getbatch(L, 2);
  CassFuture *future = cass_session_execute_batch(s->session, b->batch);
  cass_future_wait(future);
  if (cass_future_error_code(future) == CASS_OK) {
    lua_pushboolean(L, 1);
  }
  else {
    lua_pushboolean(L, 0);
  }
  cass_future_free(future);
  return 1;

}

/* TODO
* const CassSchemaMeta* cass_session_get_schema_meta(const CassSession* session)
*/
LUALIB_API int lua_cass_session_get_schema_meta(lua_State *L) {
  return 0;
}

/*
* void cass_session_get_metrics(const CassSession* session, CassMetrics* output)
*/
LUALIB_API int lua_cass_session_get_metrics(lua_State *L) {
  session_t *s = getsession(L, 1);
  CassMetrics metrics;
  cass_session_get_metrics(s->session, &metrics);
  lua_newtable(L);
  /* requests */
  lua_newtable(L);
  lua_pushnumber(L, metrics.requests.min);
  lua_setfield(L, -2, "min");
  lua_pushnumber(L, metrics.requests.max);
  lua_setfield(L, -2, "max");
  lua_pushnumber(L, metrics.requests.mean);
  lua_setfield(L, -2, "mean");
  lua_pushnumber(L, metrics.requests.stddev);
  lua_setfield(L, -2, "stddev");
  lua_pushnumber(L, metrics.requests.median);
  lua_setfield(L, -2, "median");
  lua_pushnumber(L, metrics.requests.percentile_75th);
  lua_setfield(L, -2, "percentile_75th");
  lua_pushnumber(L, metrics.requests.percentile_95th);
  lua_setfield(L, -2, "percentile_95th");
  lua_pushnumber(L, metrics.requests.percentile_98th);
  lua_setfield(L, -2, "percentile_98th");
  lua_pushnumber(L, metrics.requests.percentile_99th);
  lua_setfield(L, -2, "percentile_99th");
  lua_pushnumber(L, metrics.requests.percentile_999th);
  lua_setfield(L, -2, "percentile_999th");
  lua_pushnumber(L, metrics.requests.mean_rate);
  lua_setfield(L, -2, "mean_rate");
  lua_pushnumber(L, metrics.requests.one_minute_rate);
  lua_setfield(L, -2, "one_minute_rate");
  lua_pushnumber(L, metrics.requests.five_minute_rate);
  lua_setfield(L, -2, "five_minute_rate");
  lua_pushnumber(L, metrics.requests.fifteen_minute_rate);
  lua_setfield(L, -2, "fifteen_minute_rate");
  lua_setfield(L, -2, "requests");
  /* stats */
  lua_newtable(L);
  lua_pushnumber(L, metrics.stats.total_connections);
  lua_setfield(L, -2, "total_connections");
  lua_pushnumber(L, metrics.stats.available_connections);
  lua_setfield(L, -2, "available_connections");
  lua_pushnumber(L, metrics.stats.exceeded_pending_requests_water_mark);
  lua_setfield(L, -2, "exceeded_pending_requests_water_mark");
  lua_pushnumber(L, metrics.stats.exceeded_write_bytes_water_mark);
  lua_setfield(L, -2, "exceeded_write_bytes_water_mark");
  lua_setfield(L, -2, "stats");
  /* errors */
  lua_newtable(L);
  lua_pushnumber(L, metrics.errors.connection_timeouts);
  lua_setfield(L, -2, "connection_timeouts");
  lua_pushnumber(L, metrics.errors.pending_request_timeouts);
  lua_setfield(L, -2, "pending_request_timeouts");
  lua_pushnumber(L, metrics.errors.request_timeouts);
  lua_setfield(L, -2, "request_timeouts");
  lua_setfield(L, -2, "errors");
  return 1;
}


static luaL_Reg session_module[] = {
  { "session", lua_cass_session_new },
  { NULL, NULL }
};

LUALIB_API int lua_cass_session_open(lua_State *L) {
  createmeta(L, "session", session_reg);
  luaL_setfuncs(L, session_module, 0);
  return 0;
}

