#ifndef LUACASS_RESULT_H
#define LUACASS_RESULT_H

#include <stdlib.h>
#include "lua.h"
#include "lauxlib.h"
#include <cassandra.h>
#include "structs.h"
#include "helpers.h"
#include "iterator.h"

LUALIB_API int lua_cass_result_open(lua_State *L);
LUALIB_API int lua_cass_result_new(lua_State *L, CassFuture *future);
LUALIB_API int lua_cass_result_free(lua_State *L);
LUALIB_API int lua_cass_result_row_count(lua_State *L);
LUALIB_API int lua_cass_result_column_count(lua_State *L);
LUALIB_API int lua_cass_result_column_name(lua_State *L);
LUALIB_API int lua_cass_result_has_more_pages(lua_State *L);
LUALIB_API int lua_cass_result_paging_state_token(lua_State *L);

static const luaL_reg result_reg[] = {
  { "free", lua_cass_result_free },
  { "row_count", lua_cass_result_row_count },
  { "column_count", lua_cass_result_column_count },
  { "column_name", lua_cass_result_column_name },
  { "has_more_pages", lua_cass_result_has_more_pages },
  { "paging_state_token", lua_cass_result_paging_state_token },
  { "iterator", lua_cass_iterator_from_result },
  { "__gc", lua_cass_result_free },
  { NULL, NULL}
};

#endif
