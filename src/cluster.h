#ifndef LUACASS_CLUSTER_H
#define LUACASS_CLUSTER_H

#include "lua.h"
#include "lauxlib.h"
#include <cassandra.h>
#include "structs.h"
#include "helpers.h"
#include "ssl.h"
LUALIB_API int lua_cass_cluster_open(lua_State *L);

LUALIB_API int lua_cass_cluster_new(lua_State *L);
LUALIB_API int lua_cass_cluster_free(lua_State *L);
LUALIB_API int lua_cass_cluster_set_contact_points(lua_State *L);
LUALIB_API int lua_cass_cluster_set_port(lua_State *L);
LUALIB_API int lua_cass_cluster_set_ssl(lua_State *L);
LUALIB_API int lua_cass_cluster_set_protocol_version(lua_State *L);
LUALIB_API int lua_cass_cluster_set_num_threads_io(lua_State *L);
LUALIB_API int lua_cass_cluster_set_queue_size_io(lua_State *L);
LUALIB_API int lua_cass_cluster_set_queue_size_event(lua_State *L);
LUALIB_API int lua_cass_cluster_set_queue_size_log(lua_State *L);
LUALIB_API int lua_cass_cluster_set_core_connections_per_host(lua_State *L);
LUALIB_API int lua_cass_cluster_set_max_connections_per_host(lua_State *L);
LUALIB_API int lua_cass_cluster_set_reconnect_wait_time(lua_State *L);
LUALIB_API int lua_cass_cluster_set_max_concurrent_creation(lua_State *L);
LUALIB_API int lua_cass_cluster_set_max_concurrent_requests_threshold(lua_State *L);
LUALIB_API int lua_cass_cluster_set_max_requests_per_flush(lua_State *L);
LUALIB_API int lua_cass_cluster_set_write_bytes_high_water_mark(lua_State *L);
LUALIB_API int lua_cass_cluster_set_write_bytes_low_water_mark(lua_State *L);
LUALIB_API int lua_cass_cluster_set_pending_requests_high_water_mark(lua_State *L);
LUALIB_API int lua_cass_cluster_set_pending_requests_low_water_mark(lua_State *L);
LUALIB_API int lua_cass_cluster_set_connect_timeout(lua_State *L);
LUALIB_API int lua_cass_cluster_set_request_timeout(lua_State *L);
LUALIB_API int lua_cass_cluster_set_credentials(lua_State *L);
LUALIB_API int lua_cass_cluster_set_load_balance_round_robin(lua_State *L);
LUALIB_API int lua_cass_cluster_set_load_balance_dc_aware(lua_State *L);
LUALIB_API int lua_cass_cluster_set_token_aware_routing(lua_State *L);
LUALIB_API int lua_cass_cluster_set_latency_aware_routing(lua_State *L);
LUALIB_API int lua_cass_cluster_set_latency_aware_routing_settings(lua_State *L);
LUALIB_API int lua_cass_cluster_set_whitelist_filtering(lua_State *L);
LUALIB_API int lua_cass_cluster_set_blacklist_filtering(lua_State *L);
LUALIB_API int lua_cass_cluster_set_whitelist_dc_filtering(lua_State *L);
LUALIB_API int lua_cass_cluster_set_blacklist_dc_filtering(lua_State *L);
LUALIB_API int lua_cass_cluster_set_tcp_nodelay(lua_State *L);
LUALIB_API int lua_cass_cluster_set_tcp_keepalive(lua_State *L);
LUALIB_API int lua_cass_cluster_set_timestamp_gen(lua_State *L);
LUALIB_API int lua_cass_cluster_set_connection_heartbeat_interval(lua_State *L);
LUALIB_API int lua_cass_cluster_set_connection_idle_timeout(lua_State *L);
LUALIB_API int lua_cass_cluster_set_retry_policy(lua_State *L);
LUALIB_API int lua_cass_cluster_set_use_schema(lua_State *L);


static const struct luaL_reg cluster_reg[] = {
  /* cluster */
  { "new", lua_cass_cluster_new },
  { "free", lua_cass_cluster_free },
  { "set_contact_points", lua_cass_cluster_set_contact_points },
  { "set_port", lua_cass_cluster_set_port },
  { "set_ssl", lua_cass_cluster_set_ssl },
  { "set_protocol_version", lua_cass_cluster_set_protocol_version },
  { "set_num_threads_io", lua_cass_cluster_set_num_threads_io },
  { "set_queue_size_io", lua_cass_cluster_set_queue_size_io },
  { "set_queue_size_event", lua_cass_cluster_set_queue_size_event },
  { "set_queue_size_log", lua_cass_cluster_set_queue_size_log },
  { "set_core_connections_per_host", lua_cass_cluster_set_core_connections_per_host },
  { "set_max_connections_per_host", lua_cass_cluster_set_max_connections_per_host },
  { "set_reconnect_wait_time", lua_cass_cluster_set_reconnect_wait_time },
  { "set_max_concurrent_creation", lua_cass_cluster_set_max_concurrent_creation },
  { "set_max_concurrent_requests_threshold", lua_cass_cluster_set_max_concurrent_requests_threshold },
  { "set_max_requests_per_flush", lua_cass_cluster_set_max_requests_per_flush },
  { "set_write_bytes_high_water_mark", lua_cass_cluster_set_write_bytes_high_water_mark },
  { "set_write_bytes_low_water_mark", lua_cass_cluster_set_write_bytes_low_water_mark },
  { "set_pending_requests_high_water_mark", lua_cass_cluster_set_pending_requests_high_water_mark },
  { "set_pending_requests_low_water_mark", lua_cass_cluster_set_pending_requests_low_water_mark },
  { "set_connect_timeout", lua_cass_cluster_set_connect_timeout },
  { "set_request_timeout", lua_cass_cluster_set_request_timeout },
  { "set_credentials", lua_cass_cluster_set_credentials },
  { "set_load_balance_round_robin", lua_cass_cluster_set_load_balance_round_robin },
  { "set_load_balance_dc_aware", lua_cass_cluster_set_load_balance_dc_aware },
  { "set_token_aware_routing", lua_cass_cluster_set_token_aware_routing },
  { "set_latency_aware_routing", lua_cass_cluster_set_latency_aware_routing },
  { "set_latency_aware_routing_settings", lua_cass_cluster_set_latency_aware_routing_settings },
  { "set_whitelist_filtering", lua_cass_cluster_set_whitelist_filtering },
  { "set_blacklist_filtering", lua_cass_cluster_set_blacklist_filtering },
  { "set_whitelist_dc_filtering", lua_cass_cluster_set_whitelist_dc_filtering },
  { "set_blacklist_dc_filtering", lua_cass_cluster_set_blacklist_dc_filtering },
  { "set_tcp_nodelay", lua_cass_cluster_set_tcp_nodelay },
  { "set_tcp_keepalive", lua_cass_cluster_set_tcp_keepalive },
  { "set_timestamp_gen", lua_cass_cluster_set_timestamp_gen },
  { "set_connection_heartbeat_interval", lua_cass_cluster_set_connection_heartbeat_interval },
  { "set_connection_idle_timeout", lua_cass_cluster_set_connection_idle_timeout },
  { "set_retry_policy", lua_cass_cluster_set_retry_policy },
  { "set_use_schema", lua_cass_cluster_set_use_schema },
  { "__gc", lua_cass_cluster_free },
  { NULL, NULL }
};

#endif
