#include "cluster.h"
/*
* CassCluster * cass_cluster_new()
*/
LUALIB_API int lua_cass_cluster_new(lua_State *L) {
  cluster_t *c = (cluster_t *)lua_newuserdata(L, sizeof(cluster_t));
  c->cluster = cass_cluster_new();
  setmeta(L, "cluster");
  return 1;
}

/*
* void cass_cluster_free(CassCluster* cluster)
*/
LUALIB_API int lua_cass_cluster_free(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  if(c != NULL && c->cluster != NULL) {
    cass_cluster_free(c->cluster);
    c->cluster = NULL;
  }
  lua_pushnumber(L, 1);
  return 1;
}

/*
* CassError cass_cluster_set_contact_points(CassCluster* cluster, const char* contact_points)
*/
LUALIB_API int lua_cass_cluster_set_contact_points(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  const char *contact_points = lua_tostring(L, 2);
  CassError ce = cass_cluster_set_contact_points(c->cluster, contact_points);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_cluster_set_port(CassCluster* cluster, int port)
*/
LUALIB_API int lua_cass_cluster_set_port(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  int port = lua_tonumber(L, 2);
  CassError ce = cass_cluster_set_port(c->cluster, port);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/* TODO
* void cass_cluster_set_ssl(CassCluster* cluster, CassSsl* ssl)
*/
LUALIB_API int lua_cass_cluster_set_ssl(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  ssl_t *s = getssl(L, 2);
  cass_cluster_set_ssl(c->cluster, s->ssl);
  return 0;
}

/*
* CassError cass_cluster_set_protocol_version(CassCluster* cluster, int protocol_version)
*/
LUALIB_API int lua_cass_cluster_set_protocol_version(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  int protocol_version = lua_tonumber(L, 2);
  CassError ce = cass_cluster_set_protocol_version(c->cluster, protocol_version);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_cluster_set_num_threads_io(CassCluster* cluster, unsigned num_threads)
*/
LUALIB_API int lua_cass_cluster_set_num_threads_io(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned num_threads = (unsigned) lua_tonumber(L, 2);
  CassError ce = cass_cluster_set_num_threads_io(c->cluster, num_threads);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_cluster_set_queue_size_io(CassCluster* cluster, unsigned queue_size)
*/
LUALIB_API int lua_cass_cluster_set_queue_size_io(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned queue_size = (unsigned) lua_tonumber(L, 2);
  CassError ce = cass_cluster_set_queue_size_io(c->cluster, queue_size);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_cluster_set_queue_size_event(CassCluster* cluster, unsigned queue_size)
*/
LUALIB_API int lua_cass_cluster_set_queue_size_event(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned queue_size = (unsigned) lua_tonumber(L, 2);
  CassError ce = cass_cluster_set_queue_size_event(c->cluster, queue_size);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/* TODO
* CassError cass_cluster_set_queue_size_log(CassCluster* cluster, unsigned queue_size)
*/
LUALIB_API int lua_cass_cluster_set_queue_size_log(lua_State *L) {
  return 1;
}

/*
* CassError cass_cluster_set_core_connections_per_host(CassCluster* cluster, unsigned num_connections)
*/
LUALIB_API int lua_cass_cluster_set_core_connections_per_host(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned num_connections = (unsigned) lua_tonumber(L, 2);
  CassError ce = cass_cluster_set_core_connections_per_host(c->cluster, num_connections);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_cluster_set_max_connections_per_host(CassCluster* cluster, unsigned num_connections)
*/
LUALIB_API int lua_cass_cluster_set_max_connections_per_host(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned num_connections = (unsigned) lua_tonumber(L, 2);
  CassError ce = cass_cluster_set_max_connections_per_host(c->cluster, num_connections);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* void cass_cluster_set_reconnect_wait_time(CassCluster* cluster, unsigned wait_time)
*/
LUALIB_API int lua_cass_cluster_set_reconnect_wait_time(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned wait_time = (unsigned) lua_tonumber(L, 2);
  cass_cluster_set_reconnect_wait_time(c->cluster, wait_time);
  return 0;
}

/*
* CassError cass_cluster_set_max_concurrent_creation(CassCluster* cluster, unsigned num_connections)
*/
LUALIB_API int lua_cass_cluster_set_max_concurrent_creation(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned num_connections = (unsigned) lua_tonumber(L, 2);
  CassError ce = cass_cluster_set_max_concurrent_creation(c->cluster, num_connections);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_cluster_set_max_concurrent_requests_threshold(CassCluster* cluster, unsigned num_requests)
*/
LUALIB_API int lua_cass_cluster_set_max_concurrent_requests_threshold(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned num_requests = (unsigned) lua_tonumber(L, 2);
  CassError ce = cass_cluster_set_max_concurrent_requests_threshold(c->cluster, num_requests);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_cluster_set_max_requests_per_flush(CassCluster* cluster, unsigned num_requests)
*/
LUALIB_API int lua_cass_cluster_set_max_requests_per_flush(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned num_requests = (unsigned) lua_tonumber(L, 2);
  CassError ce = cass_cluster_set_max_requests_per_flush(c->cluster, num_requests);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_cluster_set_write_bytes_high_water_mark(CassCluster* cluster, unsigned num_bytes)
*/
LUALIB_API int lua_cass_cluster_set_write_bytes_high_water_mark(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned num_bytes = (unsigned) lua_tonumber(L, 2);
  CassError ce = cass_cluster_set_write_bytes_high_water_mark(c->cluster, num_bytes);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_cluster_set_write_bytes_low_water_mark(CassCluster* cluster, unsigned num_bytes)
*/
LUALIB_API int lua_cass_cluster_set_write_bytes_low_water_mark(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned num_bytes = (unsigned) lua_tonumber(L, 2);
  CassError ce = cass_cluster_set_write_bytes_low_water_mark(c->cluster, num_bytes);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_cluster_set_pending_requests_high_water_mark(CassCluster* cluster, unsigned num_requests)
*/
LUALIB_API int lua_cass_cluster_set_pending_requests_high_water_mark(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned num_requests = (unsigned) lua_tonumber(L, 2);
  CassError ce = cass_cluster_set_pending_requests_high_water_mark(c->cluster, num_requests);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_cluster_set_pending_requests_low_water_mark(CassCluster* cluster, unsigned num_requests)
*/
LUALIB_API int lua_cass_cluster_set_pending_requests_low_water_mark(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned num_requests = (unsigned) lua_tonumber(L, 2);
  CassError ce = cass_cluster_set_pending_requests_low_water_mark(c->cluster, num_requests);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* void cass_cluster_set_connect_timeout(CassCluster* cluster, unsigned timeout_ms)
*/
LUALIB_API int lua_cass_cluster_set_connect_timeout(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned timeout_ms = (unsigned) lua_tonumber(L, 2);
  cass_cluster_set_connect_timeout(c->cluster, timeout_ms);
  return 0;
}

/*
* void cass_cluster_set_request_timeout(CassCluster* cluster, unsigned timeout_ms)
*/
LUALIB_API int lua_cass_cluster_set_request_timeout(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned timeout_ms = (unsigned) lua_tonumber(L, 2);
  cass_cluster_set_request_timeout(c->cluster, timeout_ms);
  return 0;
}

/*
* void cass_cluster_set_credentials(CassCluster* cluster, const char* username, const char* password)
*/
LUALIB_API int lua_cass_cluster_set_credentials(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  const char *username = lua_tostring(L, 2);
  const char *password = lua_tostring(L, 3);
  cass_cluster_set_credentials(c->cluster, username, password);
  return 0;
}

/*
* void cass_cluster_set_load_balance_round_robin(CassCluster* cluster)
*/
LUALIB_API int lua_cass_cluster_set_load_balance_round_robin(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  cass_cluster_set_load_balance_round_robin(c->cluster);
  return 0;
}

/*
* CassError cass_cluster_set_load_balance_dc_aware(CassCluster* cluster, const char* local_dc, unsigned used_hosts_per_remote_dc, cass_bool_t allow_remote_dcs_for_local_cl)
*/
LUALIB_API int lua_cass_cluster_set_load_balance_dc_aware(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  const char *local_dc = lua_tostring(L, 2);
  unsigned used_hosts_per_remote_dc = (unsigned) lua_tonumber(L, 3);
  cass_bool_t allow_remote_dcs_for_local_cl = (cass_bool_t) lua_toboolean(L, 4);
  CassError ce = cass_cluster_set_load_balance_dc_aware(c->cluster, local_dc, used_hosts_per_remote_dc, allow_remote_dcs_for_local_cl);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* void cass_cluster_set_token_aware_routing(CassCluster* cluster, cass_bool_t enabled)
*/
LUALIB_API int lua_cass_cluster_set_token_aware_routing(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  cass_bool_t enabled = (cass_bool_t) lua_toboolean(L, 2);
  cass_cluster_set_token_aware_routing(c->cluster, enabled);
  return 0;
}

/*
* void cass_cluster_set_latency_aware_routing(CassCluster* cluster, cass_bool_t enabled)
*/
LUALIB_API int lua_cass_cluster_set_latency_aware_routing(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  cass_bool_t enabled = (cass_bool_t) lua_toboolean(L, 2);
  cass_cluster_set_latency_aware_routing(c->cluster, enabled);
  return 0;
}

/* TODO
* void cass_cluster_set_latency_aware_routing_settings(CassCluster* cluster, cass_double_t exclusion_threshold, cass_uint64_t scale_ms, cass_uint64_t retry_period_ms, cass_uint64_t update_rate_ms, cass_uint64_t min_measured)
*/
LUALIB_API int lua_cass_cluster_set_latency_aware_routing_settings(lua_State *L) {
  return 0;
}

/*
* void cass_cluster_set_whitelist_filtering(CassCluster* cluster, const char* hosts)
*/
LUALIB_API int lua_cass_cluster_set_whitelist_filtering(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  const char *hosts = lua_tostring(L, 2);
  cass_cluster_set_whitelist_filtering(c->cluster, hosts);
  return 0;
}

/*
* void cass_cluster_set_blacklist_filtering(CassCluster* cluster, const char* hosts)
*/
LUALIB_API int lua_cass_cluster_set_blacklist_filtering(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  const char *hosts = lua_tostring(L, 2);
  cass_cluster_set_blacklist_filtering(c->cluster, hosts);
  return 0;
}

/*
* void cass_cluster_set_whitelist_dc_filtering(CassCluster* cluster, const char* dcs)
*/
LUALIB_API int lua_cass_cluster_set_whitelist_dc_filtering(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  const char *dcs = lua_tostring(L, 2);
  cass_cluster_set_whitelist_dc_filtering(c->cluster, dcs);
  return 0;
}

/*
* void cass_cluster_set_blacklist_dc_filtering(CassCluster* cluster, const char* dcs)
*/
LUALIB_API int lua_cass_cluster_set_blacklist_dc_filtering(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  const char *dcs = lua_tostring(L, 2);
  cass_cluster_set_blacklist_dc_filtering(c->cluster, dcs);
  return 0;
}

/*
* void cass_cluster_set_tcp_nodelay(CassCluster* cluster, cass_bool_t enabled)
*/
LUALIB_API int lua_cass_cluster_set_tcp_nodelay(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  cass_bool_t enabled = (cass_bool_t) lua_toboolean(L, 2);
  cass_cluster_set_tcp_nodelay(c->cluster, enabled);
  return 0;
}

/*
* void cass_cluster_set_tcp_keepalive(CassCluster* cluster, cass_bool_t enabled, unsigned delay_secs)
*/
LUALIB_API int lua_cass_cluster_set_tcp_keepalive(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  cass_bool_t enabled = (cass_bool_t) lua_toboolean(L, 2);
  unsigned delay_secs = (unsigned) lua_tonumber(L, 3);
  cass_cluster_set_tcp_keepalive(c->cluster, enabled, delay_secs);
  return 0;
}

/* TODO
* void cass_cluster_set_timestamp_gen(CassCluster* cluster, CassTimestampGen* timestamp_gen)
*/
LUALIB_API int lua_cass_cluster_set_timestamp_gen(lua_State *L) {
  return 0;
}

/*
* void cass_cluster_set_connection_heartbeat_interval(CassCluster* cluster, unsigned interval_secs)
*/
LUALIB_API int lua_cass_cluster_set_connection_heartbeat_interval(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned interval_secs = (unsigned) lua_tonumber(L, 2);
  cass_cluster_set_connection_heartbeat_interval(c->cluster, interval_secs);
  return 0;
}

/*
* void cass_cluster_set_connection_idle_timeout(CassCluster* cluster, unsigned timeout_secs)
*/
LUALIB_API int lua_cass_cluster_set_connection_idle_timeout(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  unsigned timeout_secs = (unsigned) lua_tonumber(L, 2);
  cass_cluster_set_connection_idle_timeout(c->cluster, timeout_secs);
  return 0;
}

/*
* void cass_cluster_set_retry_policy(CassCluster* cluster, CassRetryPolicy* retry_policy)
*/
LUALIB_API int lua_cass_cluster_set_retry_policy(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  CassRetryPolicy *retry_policy = (CassRetryPolicy *) lua_touserdata(L, 2);
  cass_cluster_set_retry_policy(c->cluster, retry_policy);
  return 0;
}

/*
* void cass_cluster_set_use_schema(CassCluster* cluster, cass_bool_t enabled)
*/
LUALIB_API int lua_cass_cluster_set_use_schema(lua_State *L) {
  cluster_t *c = getcluster(L, 1);
  cass_bool_t enabled = (cass_bool_t) lua_toboolean(L, 2);
  cass_cluster_set_use_schema(c->cluster, enabled);
  return 0;
}


static luaL_Reg cluster_module[] = {
  { "cluster", lua_cass_cluster_new },
  { NULL, NULL }
};

LUALIB_API int lua_cass_cluster_open(lua_State *L) {
  createmeta(L, "cluster", cluster_reg);
  luaL_setfuncs(L, cluster_module, 0);
  return 0;
}

