#include "statement.h"
/*
* CassStatement* cass_statement_new(const char* query, size_t parameter_count)
*/
LUALIB_API int lua_cass_statement_new(lua_State *L) {
  statement_t *s = (statement_t *) lua_newuserdata(L, sizeof(statement_t));
  const char *query = lua_tostring(L, 1);
  size_t parameter_count = lua_tonumber(L, 2);
  s->statement = cass_statement_new(query, parameter_count);
  s->prepared = NULL;
  setmeta(L, "statement");
  return 1;
}

LUALIB_API int lua_cass_statement_new_from_prepared(lua_State *L, const CassPrepared *prepared) {
  statement_t *s = (statement_t *) lua_newuserdata(L, sizeof(statement_t));
  s->statement = cass_prepared_bind(prepared);
  s->prepared = prepared;
  setmeta(L, "statement");
  return 1;
}

/*
* void cass_statement_free(CassStatement* statement)
*/
LUALIB_API int lua_cass_statement_free(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  if(s != NULL && s->statement != NULL) {
    cass_statement_free(s->statement);
    s->statement = NULL;
  }
  if(s != NULL && s->prepared != NULL) {
    cass_prepared_free(s->prepared);
    s->prepared = NULL;
  }
  lua_pushnumber(L, 1);
  return 1;
}

/*
* CassError cass_statement_add_key_index(CassStatement* statement, size_t index)
*/
LUALIB_API int lua_cass_statement_add_key_index(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  size_t index = lua_tonumber(L, 2);
  CassError ce = cass_statement_add_key_index(s->statement, index);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_set_keyspace(CassStatement* statement, const char* keyspace)
*/
LUALIB_API int lua_cass_statement_set_keyspace(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  const char *keyspace = lua_tostring(L, 2);
  CassError ce = cass_statement_set_keyspace(s->statement, keyspace);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_set_consistency(CassStatement* statement, CassConsistency consistency)
*/
LUALIB_API int lua_cass_statement_set_consistency(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  CassConsistency consistency = lua_tonumber(L, 2);
  CassError ce = cass_statement_set_consistency(s->statement, consistency);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/* TODO
* CassError cass_statement_set_serial_consistency(CassStatement* statement, CassConsistency serial_consistency)
*/
LUALIB_API int lua_cass_statement_set_serial_consistency(lua_State *L) {
  return 1;
}

/*
* CassError cass_statement_set_paging_size(CassStatement* statement, int page_size)
*/
LUALIB_API int lua_cass_statement_set_paging_size(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  int page_size = lua_tonumber(L, 2);
  CassError ce = cass_statement_set_paging_size(s->statement, page_size);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/* TODO
* CassError cass_statement_set_paging_state(CassStatement* statement, const CassResult* result)
*/
LUALIB_API int lua_cass_statement_set_paging_state(lua_State *L) {
  return 1;
}

/*
* CassError cass_statement_set_paging_state_token(CassStatement* statement, const char* paging_state, size_t paging_state_size)
*/
LUALIB_API int lua_cass_statement_set_paging_state_token(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  const char *paging_state = lua_tostring(L, 2);
  size_t paging_state_size = lua_tonumber(L, 3);
  CassError ce = cass_statement_set_paging_state_token(s->statement, paging_state, paging_state_size);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_set_timestamp(CassStatement* statement, cass_int64_t timestamp)
*/
LUALIB_API int lua_cass_statement_set_timestamp(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  cass_int64_t timestamp = getint64(L, 2);
  CassError ce = cass_statement_set_timestamp(s->statement, timestamp);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/* TODO
* CassError cass_statement_set_retry_policy(CassStatement* statement, CassRetryPolicy* retry_policy)
*/
LUALIB_API int lua_cass_statement_set_retry_policy(lua_State *L) {
  return 1;
}

/* TODO
* CassError cass_statement_set_custom_payload(CassStatement* statement, const CassCustomPayload* payload)
*/
LUALIB_API int lua_cass_statement_set_custom_payload(lua_State *L) {
  return 1;
}

/*
* CassError cass_statement_bind_null(CassStatement* statement, size_t index)
*/
LUALIB_API int lua_cass_statement_bind_null(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  size_t index = luaL_checknumber(L, 2);
  CassError ce = cass_statement_bind_null(s->statement, index);
  if(ce == CASS_OK) {
    lua_pushboolean(L, 1);
  }
  else {
    lua_pushboolean(L, 0);
  }
  return 1;
}

/*
* CassError cass_statement_bind_null_by_name(CassStatement* statement, const char* name)
*/
LUALIB_API int lua_cass_statement_bind_null_by_name(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  const char *name = lua_tostring(L, 2);
  CassError ce = cass_statement_bind_null_by_name(s->statement, name);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_int8(CassStatement* statement, size_t index, cass_int8_t value)
*/
LUALIB_API int lua_cass_statement_bind_int8(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  size_t index = lua_tonumber(L, 2);
  cass_int8_t value = (cass_int8_t) lua_tonumber(L, 3);
  CassError ce = cass_statement_bind_int8(s->statement, index, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_int8_by_name(CassStatement* statement, const char* name, cass_int8_t value)
*/
LUALIB_API int lua_cass_statement_bind_int8_by_name(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  const char *name = lua_tostring(L, 2);
  cass_int8_t value = (cass_int8_t) lua_tonumber(L, 3);
  CassError ce = cass_statement_bind_int8_by_name(s->statement, name, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_int16(CassStatement* statement, size_t index, cass_int16_t value)
*/
LUALIB_API int lua_cass_statement_bind_int16(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  size_t index = lua_tonumber(L, 2);
  cass_int16_t value = (cass_int16_t) lua_tonumber(L, 3);
  CassError ce = cass_statement_bind_int16(s->statement, index, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_int16_by_name(CassStatement* statement, const char* name, cass_int16_t value)
*/
LUALIB_API int lua_cass_statement_bind_int16_by_name(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  const char *name = lua_tostring(L, 2);
  cass_int16_t value = (cass_int16_t) lua_tonumber(L, 3);
  CassError ce = cass_statement_bind_int16_by_name(s->statement, name, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_int32(CassStatement* statement, size_t index, cass_int32_t value)
*/
LUALIB_API int lua_cass_statement_bind_int32(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  size_t index = lua_tonumber(L, 2);
  cass_int32_t value = (cass_int32_t) lua_tonumber(L, 3);
  CassError ce = cass_statement_bind_int32(s->statement, index, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_int32_by_name(CassStatement* statement, const char* name, cass_int32_t value)
*/
LUALIB_API int lua_cass_statement_bind_int32_by_name(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  const char *name = lua_tostring(L, 2);
  cass_int32_t value = (cass_int32_t) lua_tonumber(L, 3);
  CassError ce = cass_statement_bind_int32_by_name(s->statement, name, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_uint32(CassStatement* statement, size_t index, cass_uint32_t value)
*/
LUALIB_API int lua_cass_statement_bind_uint32(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  size_t index = lua_tonumber(L, 2);
  cass_uint32_t value = (cass_uint32_t) lua_tonumber(L, 3);
  CassError ce = cass_statement_bind_uint32(s->statement, index, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_uint32_by_name(CassStatement* statement, const char* name, cass_uint32_t value)
*/
LUALIB_API int lua_cass_statement_bind_uint32_by_name(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  const char *name = lua_tostring(L, 2);
  cass_uint32_t value = (cass_uint32_t) lua_tonumber(L, 3);
  CassError ce = cass_statement_bind_uint32_by_name(s->statement, name, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_int64(CassStatement* statement, size_t index, cass_int64_t value)
*/
LUALIB_API int lua_cass_statement_bind_int64(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  size_t index = lua_tonumber(L, 2);
  cass_int64_t value = (cass_int64_t) luaL_checknumber(L, 3);
  CassError ce = cass_statement_bind_int64(s->statement, index, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_int64_by_name(CassStatement* statement, const char* name, cass_int64_t value)
*/
LUALIB_API int lua_cass_statement_bind_int64_by_name(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  const char *name = lua_tostring(L, 2);
  cass_int64_t value = getint64(L, 3);
  CassError ce = cass_statement_bind_int64_by_name(s->statement, name, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_float(CassStatement* statement, size_t index, cass_float_t value)
*/
LUALIB_API int lua_cass_statement_bind_float(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  size_t index = lua_tonumber(L, 2);
  cass_float_t value = (cass_float_t) lua_tonumber(L, 3);
  CassError ce = cass_statement_bind_float(s->statement, index, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_float_by_name(CassStatement* statement, const char* name, cass_float_t value)
*/
LUALIB_API int lua_cass_statement_bind_float_by_name(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  const char *name = lua_tostring(L, 2);
  cass_float_t value = (cass_float_t) lua_tonumber(L, 3);
  CassError ce = cass_statement_bind_float_by_name(s->statement, name, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_double(CassStatement* statement, size_t index, cass_double_t value)
*/
LUALIB_API int lua_cass_statement_bind_double(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  size_t index = lua_tonumber(L, 2);
  cass_double_t value = (cass_double_t) lua_tonumber(L, 3);
  CassError ce = cass_statement_bind_double(s->statement, index, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_double_by_name(CassStatement* statement, const char* name, cass_double_t value)
*/
LUALIB_API int lua_cass_statement_bind_double_by_name(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  const char *name = lua_tostring(L, 2);
  cass_double_t value = (cass_double_t) lua_tonumber(L, 3);
  CassError ce = cass_statement_bind_double_by_name(s->statement, name, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_bool(CassStatement* statement, size_t index, cass_bool_t value)
*/
LUALIB_API int lua_cass_statement_bind_bool(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  size_t index = lua_tonumber(L, 2);
  cass_bool_t value = (cass_bool_t) lua_toboolean(L, 3);
  CassError ce = cass_statement_bind_bool(s->statement, index, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_bool_by_name(CassStatement* statement, const char* name, cass_bool_t value)
*/
LUALIB_API int lua_cass_statement_bind_bool_by_name(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  const char *name = lua_tostring(L, 2);
  cass_bool_t value = (cass_bool_t) lua_toboolean(L, 3);
  CassError ce = cass_statement_bind_bool_by_name(s->statement, name, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}
/*
* CassError cass_statement_bind_string(CassStatement* statement, size_t index, const char* value)
*/
LUALIB_API int lua_cass_statement_bind_string(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  size_t index = lua_tonumber(L, 2);
  const char *value = lua_tostring(L, 3);
  CassError ce = cass_statement_bind_string(s->statement, index, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_string_by_name(CassStatement* statement, const char* name, const char* value)
*/
LUALIB_API int lua_cass_statement_bind_string_by_name(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  const char *name = lua_tostring(L, 2);
  const char *value = lua_tostring(L, 3);
  CassError ce = cass_statement_bind_string_by_name(s->statement, name, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/* TODO
* CassError cass_statement_bind_bytes(CassStatement* statement, size_t index, const cass_byte_t* value, size_t value_size)
*/
LUALIB_API int lua_cass_statement_bind_bytes(lua_State *L) {
  return 1;
}

/* TODO
* CassError cass_statement_bind_bytes_by_name(CassStatement* statement, const char* name, const cass_byte_t* value, size_t value_size)
*/
LUALIB_API int lua_cass_statement_bind_bytes_by_name(lua_State *L) {
  return 1;
}

/*
* CassError cass_statement_bind_uuid(CassStatement* statement, size_t index, CassUuid value)
*/
LUALIB_API int lua_cass_statement_bind_uuid(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  size_t index = lua_tonumber(L, 2);
  CassUuid value = getuuid(L, 3);
  CassError ce = cass_statement_bind_uuid(s->statement, index, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_uuid_by_name(CassStatement* statement, const char* name, CassUuid value)
*/
LUALIB_API int lua_cass_statement_bind_uuid_by_name(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  const char *name = lua_tostring(L, 2);
  CassUuid value = getuuid(L, 3);
  CassError ce = cass_statement_bind_uuid_by_name(s->statement, name, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_inet(CassStatement* statement, size_t index, CassInet value)
*/
LUALIB_API int lua_cass_statement_bind_inet(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  size_t index = lua_tonumber(L, 2);
  CassInet value = getinet(L, 3);
  CassError ce = cass_statement_bind_inet(s->statement, index, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/*
* CassError cass_statement_bind_inet_by_name(CassStatement* statement, const char* name, CassInet value)
*/
LUALIB_API int lua_cass_statement_bind_inet_by_name(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  const char *name = lua_tostring(L, 2);
  CassInet value = getinet(L, 3);
  CassError ce = cass_statement_bind_inet_by_name(s->statement, name, value);
  lua_pushboolean(L, (ce == CASS_OK ? 1 : 0));
  return 1;
}

/* TODO
* CassError cass_statement_bind_decimal(CassStatement* statement, size_t index, const cass_byte_t* varint, size_t varint_size, cass_int32_t scale)
*/
LUALIB_API int lua_cass_statement_bind_decimal(lua_State *L) {
  return 1;
}

/* TODO
* CassError cass_statement_bind_decimal_by_name(CassStatement* statement, const char* name, const cass_byte_t* varint, size_t varint_size, cass_int32_t scale)
*/
LUALIB_API int lua_cass_statement_bind_decimal_by_name(lua_State *L) {
  return 1;
}

/* TODO
* CassError cass_statement_bind_collection(CassStatement* statement, size_t index, const CassCollection* collection)
*/
LUALIB_API int lua_cass_statement_bind_collection(lua_State *L) {
  return 1;
}

/* TODO
* CassError cass_statement_bind_collection_by_name(CassStatement* statement, const char* name, const CassCollection* collection)
*/
LUALIB_API int lua_cass_statement_bind_collection_by_name(lua_State *L) {
  return 1;
}

/* TODO
* CassError cass_statement_bind_tuple(CassStatement* statement, size_t index, const CassTuple* tuple)
*/
LUALIB_API int lua_cass_statement_bind_tuple(lua_State *L) {
  return 1;
}

/* TODO
* CassError cass_statement_bind_tuple_by_name(CassStatement* statement, const char* name, const CassTuple* tuple)
*/
LUALIB_API int lua_cass_statement_bind_tuple_by_name(lua_State *L) {
  return 1;
}

/* TODO
* CassError cass_statement_bind_user_type(CassStatement* statement, size_t index, const CassUserType* user_type)
*/
LUALIB_API int lua_cass_statement_bind_user_type(lua_State *L) {
  return 1;
}

/* TODO
* CassError cass_statement_bind_user_type_by_name(CassStatement* statement, const char* name, const CassUserType* user_type)
*/
LUALIB_API int lua_cass_statement_bind_user_type_by_name(lua_State *L) {
  return 1;
}

/* bind to prepared statement */
LUALIB_API int lua_cass_statement_bind(lua_State *L) {
  statement_t *s = getstatement(L, 1);
  size_t index = luaL_checknumber(L, 2);
  luaL_checkany(L, 3);
  const CassDataType *data_type = cass_prepared_parameter_data_type(s->prepared, index);
  CassValueType value_type = cass_data_type_type(data_type);
  switch(value_type) {
    case CASS_VALUE_TYPE_ASCII:
    case CASS_VALUE_TYPE_TEXT:
    case CASS_VALUE_TYPE_VARCHAR:
      lua_cass_statement_bind_string(L);
      break;
    case CASS_VALUE_TYPE_INT:
      lua_cass_statement_bind_int32(L);
      break;
    case CASS_VALUE_TYPE_TIMESTAMP:
    case CASS_VALUE_TYPE_BIGINT:
      lua_cass_statement_bind_int64(L);
      break;
    case CASS_VALUE_TYPE_UUID:
    case CASS_VALUE_TYPE_TIMEUUID:
      lua_cass_statement_bind_uuid(L);
      break;
    case CASS_VALUE_TYPE_DOUBLE:
      lua_cass_statement_bind_double(L);
      break;
    case CASS_VALUE_TYPE_FLOAT:
      lua_cass_statement_bind_float(L);
      break;
    case CASS_VALUE_TYPE_BOOLEAN:
      lua_cass_statement_bind_bool(L);
      break;
    case CASS_VALUE_TYPE_INET:
      lua_cass_statement_bind_inet(L);
      break;
    default:
      luaL_error(L, "unkown/unsupported value type!");
  }
  return 1;
}

static luaL_Reg statement_module[] = {
  { "statement", lua_cass_statement_new },
  { NULL, NULL }
};

LUALIB_API int lua_cass_statement_open(lua_State *L) {
  createmeta(L, "statement", statement_reg);
  luaL_setfuncs(L, statement_module, 0);
  return 0;
}

