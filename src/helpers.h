#ifndef LUACASS_HELPERS_H
#define LUACASS_HELPERS_H

#include <stdlib.h>
#include "lauxlib.h"
#include "structs.h"
#if !defined LUA_VERSION_NUM || LUA_VERSION_NUM==501
LUALIB_API void luaL_setfuncs (lua_State *L, const luaL_Reg *l, int nup);
#endif

LUALIB_API void setmeta(lua_State *L, const char *name);
LUALIB_API int createmeta(lua_State *L, const char *name, const luaL_Reg *methods);
LUALIB_API cluster_t *getcluster(lua_State *L, size_t index);
LUALIB_API session_t *getsession(lua_State *L, size_t index);
LUALIB_API statement_t *getstatement(lua_State *L, size_t index);
LUALIB_API result_t *getresult(lua_State *L, size_t index);
LUALIB_API iterator_t *getiterator(lua_State *L, size_t index);
LUALIB_API ssl_t *getssl(lua_State *L, size_t index);
LUALIB_API batch_t *getbatch(lua_State *L, size_t index);
LUALIB_API cass_int64_t getint64(lua_State *L, size_t index);
LUALIB_API CassInet getinet(lua_State *L, size_t index);
LUALIB_API CassUuid getuuid(lua_State *L, size_t index);
LUALIB_API char *lltoa(long long val);
#endif

