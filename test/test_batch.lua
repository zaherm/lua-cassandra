print("**** test/test_batch.lua ****")
local cassandra = require("cassandra")
local cluster = cassandra.cluster()
local session = cassandra.session()
assert(cluster:set_contact_points("127.0.0.1"))
assert(session:connect(cluster))

local row_id, result, iterator, statement, row
statement = cassandra.statement("CREATE KEYSPACE IF NOT EXISTS tests WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 3 };", 0)
session:execute(statement)

statement = cassandra.statement("DROP TABLE IF EXISTS tests.test_batch_1", 0)
session:execute(statement)
statement = cassandra.statement([[
  CREATE TABLE tests.test_batch_1(
    row_id int PRIMARY KEY,
    row_value TEXT
  )
]])
session:execute(statement)

statement = cassandra.statement("DROP TABLE IF EXISTS tests.test_batch_2", 0)
session:execute(statement)
statement = cassandra.statement([[
  CREATE TABLE tests.test_batch_2(
    row_id varchar PRIMARY KEY,
    row_value int
  )
]])
session:execute(statement)


local tests = {
  batch_1 = {
    { 1, "text for 1" },
    { 2, "text for 2" },
    { 3, "text for 3" }
  },
  batch_2 = {
    { "a", 1 },
    { "b", 2 }
  }
}
local batch = cassandra.batch()
assert(batch)
for t, values in pairs(tests) do
  print("test[batch] adding "..t)
  for _,v in ipairs(values) do
    print(v[1], v[2])
    statement = session:prepare("INSERT INTO tests.test_"..t.."(row_id, row_value) VALUES(?, ?)")
    assert(statement)
    statement:bind(0, v[1])
    statement:bind(1, v[2])
    batch:add_statement(statement)
    statement:free()
  end
end
session:execute_batch(batch)

