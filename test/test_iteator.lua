print("**** test/test_iterator.lua ****")
local cassandra = require("cassandra")

local cluster = cassandra.cluster()
local session = cassandra.session()
assert(cluster:set_contact_points("127.0.0.1"))
assert(session:connect(cluster))
local statement = cassandra.statement("SELECT * FROM test.basic_types WHERE row_boolean = false ALLOW FILTERING", 0)
assert(statement:set_keyspace("test"))
local result = session:execute(statement)
assert(result:row_count() == 0)
local iterator = result:iterator()
assert(iterator)
assert(iterator:next() == false)

local statement = cassandra.statement("SELECT * FROM test.basic_types WHERE row_boolean = true ALLOW FILTERING", 0)
assert(statement:set_keyspace("test"))
local result = session:execute(statement)
assert(result:row_count() >= 1)
local iterator = result:iterator()
assert(iterator)
assert(iterator:next() == true)
local row = iterator:get_row();
for k,v in pairs(row) do
  print(k,v)
end

