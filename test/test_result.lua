print("**** test/test_result.lua ****")
local cassandra = require("cassandra")

local cluster = cassandra.cluster()
local session = cassandra.session()
assert(cluster:set_contact_points("127.0.0.1"))
assert(session:connect(cluster))
local statement = cassandra.statement("SELECT * FROM test.basic_types LIMIT 1", 0)
assert(statement:set_keyspace("test"))
assert(statement)
local result = session:execute(statement)
assert(result:row_count() == 1)

