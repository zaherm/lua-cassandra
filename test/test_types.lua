print("**** test/test_types.lua ****")
local cassandra = require("cassandra")
local cluster = cassandra.cluster()
local session = cassandra.session()
assert(cluster:set_contact_points("127.0.0.1"))
assert(session:connect(cluster))
local tests = {
  ["int"] = 1,
  ["boolean"] = false,
  ["text"] = "Text",
  ["varchar"] = "varchar",
  ["bigint"] = 1909019201212,
  ["float"] = 1.1,
  ["ascii"] = "ascii",
  ["timestamp"] = os.time(),
  ["inet"] = "127.0.0.1",
  ["uuid"] = "cfd66ccc-d857-4e90-b1e5-df98a3d40cd0",
  ["timeuuid"] = "a4a70900-24e1-11df-8924-001ff3591711"
}
local row_id, result, iterator, statement, row
statement = cassandra.statement("CREATE KEYSPACE IF NOT EXISTS tests WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 3 };", 0)
session:execute(statement)
for k,v in pairs(tests) do
  print("testing type["..k.."] with value["..tostring(v).."]")
  statement = cassandra.statement("DROP TABLE IF EXISTS tests.table_"..k, 0)
  session:execute(statement)
  statement = cassandra.statement(string.format([[
  CREATE TABLE tests.table_%s(
    row_id int PRIMARY KEY,
    row_%s %s
  )
  ]], k, k, k), 0)
  row_id = math.floor(os.time())
  print("inserting into table_"..k.." row_id["..tostring(row_id).."]")
  session:execute(statement)
  print(string.format([[
  INSERT INTO tests.table_%s(row_%s) VALUES(?, ?)
  ]], k, k))
  statement = session:prepare(string.format([[
  INSERT INTO tests.table_%s(row_id, row_%s) VALUES(?, ?)
  ]], k, k))
  assert(statement)
  statement:bind(0, row_id)
  statement:bind(1, v)
  session:execute(statement)
  statement = session:prepare(string.format([[
  SELECT * FROM tests.table_%s WHERE row_id = ? ]], k), 1)
  statement:bind(0, row_id)
  result = session:execute(statement)
  assert(result)
  assert(result:row_count() == 1)
  iterator = result:iterator()
  assert(iterator)
  assert(iterator:next())
  row = iterator:get_row()
  assert(row)
  assert(row["row_id"] == row_id)
  assert(row["row_"..k] ~= nil)
  if k == "float" then
    assert(math.floor(row["row_"..k]) == math.floor(v))
  else
    assert(row["row_"..k] == v)
  end
end
