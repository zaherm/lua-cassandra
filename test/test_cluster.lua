local cassandra = require("cassandra")
for k,v in pairs(cassandra) do
  print(k,v)
end
local cluster = cassandra.cluster()
print("**** test/test_cluster.lua ****")
assert(cluster)
assert(cluster:set_contact_points("127.0.0.1"))
cluster:free()

