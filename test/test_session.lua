local cassandra = require("cassandra")
local cluster = cassandra.cluster()
local session = cassandra.session()
print(tostring(cluster))
print(tostring(session))
print("**** test/test_session.lua ****")
assert(cluster:set_contact_points("127.0.0.1"))
assert(cluster)
assert(session:connect(cluster))
assert(session)

local metrics = session:get_metrics()
assert(type(metrics) == "table")

for k,v in pairs(metrics) do
  print(k)
  for kk,vv in pairs(v) do
    print(string.format("\t%s\t=\t%s",kk, vv))
  end
end

session:free()
cluster:free()

