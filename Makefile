UNAME := $(shell uname)
all-tests := $(basename $(wildcard test/*.lua))

LIB_PATH= /usr/lib
INC_PATH= /usr/include
BIN_PATH= /usr/bin

LUA_LIB= -L$(LIB_PATH) -llua
LUA_INC= -I$(INC_PATH) -I$(INC_PATH)/lua

CASS_INC=
CASS_LIB= -lcassandra
EXTRACFLAGS= -std=c99 -undefined -fPIC

ifeq ($(UNAME), Darwin)
	LIB_PATH= /usr/local/lib
	INC_PATH= /usr/local/include
	BIN_PATH= /usr/local/bin
	LUA_LIB= -L$(LIB_PATH) -llua5.1
	LUA_INC= -I$(INC_PATH) -I$(INC_PATH)/lua-5.1
	EXTRACFLAGS= -std=c99 -undefined dynamic_lookup -fPIC
endif


INC= $(LUA_INC) $(CASS_INC)
LIB= $(LUA_LIB) $(CASS_LIB)
WARN= -Wall
CFLAGS= -O2 $(WARN) $(INC)

MYNAME= cassandra
MYLIB= $(MYNAME)
T= $(MYLIB).so
OBJS= src/lua_$(MYLIB).o \
			src/cluster.o \
			src/session.o \
			src/result.o \
			src/iterator.o \
			src/statement.o \
			src/helpers.o \
			src/value.o \
			src/ssl.o \
			src/batch.o

all: $T

%.o: %.c
	$(CC) $(CFLAGS) -fPIC -c -o $@ $<

$T:	$(OBJS)
	$(CC) $(CFLAGS) $(LIB) $(EXTRACFLAGS) -o $@ -shared $(OBJS)

clean:
	rm -f $T $(OBJS)

tests: $(all-tests)

test/test_%: $T
	lua $@.lua

test: clean all tests

install: $(TARGET)

